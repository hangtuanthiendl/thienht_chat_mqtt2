module chat

go 1.13

require (
	github.com/confluentinc/confluent-kafka-go v1.4.2 // indirect
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/google/uuid v1.1.1 // indirect
	github.com/gorilla/websocket v1.4.2
	github.com/hazelcast/hazelcast-go-client v0.6.0
	github.com/joho/godotenv v1.3.0
	github.com/nats-io/nats-streaming-server v0.18.0 // indirect
	github.com/nats-io/nats.go v1.10.0
	github.com/nats-io/stan.go v0.7.0
	github.com/segmentio/kafka-go v0.3.7 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
	gopkg.in/confluentinc/confluent-kafka-go.v1 v1.4.2
)
