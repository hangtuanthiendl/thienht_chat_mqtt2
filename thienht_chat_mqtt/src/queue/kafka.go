package queue

import (
	"encoding/json"
	"errors"
	"log"
	"os"

	kafka "gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
)

// KafkaQueue - x
type KafkaQueue struct {
	producer *kafka.Producer
}

// New - New session for Kafka
func New() *KafkaQueue {
	config := kafka.ConfigMap{"bootstrap.servers": os.Getenv("KAFKA_BROKER")}
	p, err := kafka.NewProducer(&config)

	if err != nil {
		log.Printf("Failed to create producer: %s\n", err)
		return nil
	}

	q := &KafkaQueue{
		producer: p,
	}
	return q
}

// PushMessageToKafkaHistory - x
func (q *KafkaQueue) PushMessageToKafkaHistory(message interface{}) error {
	topic := os.Getenv("KAFKA_HISORY_TOPIC")

	if q.producer == nil {
		return errors.New("Kafka service is not working right")
	}
	value, err := json.Marshal(message)
	if err != nil {
		log.Panicln(err)
	}

	deliveryChan := make(chan kafka.Event)
	defer close(deliveryChan)

	q.producer.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
		Value:          value,
	}, deliveryChan)

	e := <-deliveryChan
	m := e.(*kafka.Message)

	if m.TopicPartition.Error != nil {
		log.Println("Delivery to kafka failed:")
		log.Println(m.TopicPartition.Error)
		return m.TopicPartition.Error
	}
	return nil
}
