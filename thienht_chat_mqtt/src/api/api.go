package api

import (
	"bytes"
	hex "encoding/hex"
	"encoding/json"
	"log"
	"net/http"
	"os"
	"strconv"
)

// API - x
type API struct {
	token string
}

// ClientInfor - Client Information, get from Java API
type ClientInfor struct {
	LstChannels []string `json:"chatChannelUsers"`
	ClientID    []int    `json:"userId"`
	IsFirst     bool     `json:"isFirst"`
}

// GetClientInforRespone - x
type GetClientInforRespone struct {
	Err         bool         `json:"error"`
	ClientInfor *ClientInfor `json:"object"`
}

// New - Decode token base on sec websocket protocol
func New(r *http.Request) (*API, error) {
	// Get token from ws protocol
	decoded, err := hex.DecodeString(r.Header.Get("Sec-Websocket-Protocol"))
	if err != nil {
		log.Println(err)
		return nil, err
	}

	return &API{
		token: string(decoded),
	}, nil
}

// Check - Decode a token from client and return clientID (key)
func (a *API) Check() (bool, int, []string, error) {
	// Call Java API at Url
	url := os.Getenv("JAVA_URL_API_GET_USER_INFOR")

	// Body is {} - dont use nil or ""
	var jsonStr = []byte(`{}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))

	// Header - API get user information
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", a.token)
	//log.Println(req.Header)
	if err != nil {
		log.Println(err)
		return false, 0, []string{}, err
	}
	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		log.Println(err)
		return false, 0, []string{}, err
	}
	defer resp.Body.Close()

	// Parse body
	var result map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&result)

	res := result["object"].(map[string]interface{})

	var lstChannels []string = []string{}
	if res["chatChannelUsers"] != nil {

		temp := res["chatChannelUsers"].([]interface{})

		for i := range temp {
			lstChannels = append(lstChannels, temp[i].(string))
		}
		log.Println(temp)
	}

	return res["isFirst"].(bool), int(res["userId"].(float64)), lstChannels, nil

}

// CreateChannel - x
func (a *API) CreateChannel(userIDs []int) (string, error) {
	log.Println(">>> CreateChannel")
	// Call Java API at Url
	url := os.Getenv("JAVA_URL_API_CREATE_CHANNEL")

	message := map[string]interface{}{
		"chatUserId": userIDs,
	}
	jsonStr, err := json.Marshal(message)
	if err != nil {
		log.Fatalln(err)
		return "", err
	}

	// Create new post request
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	// Header - API get user information
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", a.token)
	log.Println(">>> SEND REQUEST:")
	//log.Println(req)
	//
	if err != nil {
		log.Println(err)
		return "", err
	}
	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil || resp == nil {
		log.Println(err)
		return "", err
	}

	// Parse body
	var result map[string]interface{}
	log.Println(resp.Body)
	json.NewDecoder(resp.Body).Decode(&result)

	log.Println(result)
	if result["object"] == nil {
		return "", err
	}
	res := result["object"].(map[string]interface{})
	defer resp.Body.Close()
	return res["channelName"].(string), nil
}

// SyncMessageByChannel - x
func (a *API) SyncMessageByChannel(lstChannel []string) []interface{} {
	message := map[string]interface{}{
		"channelNames": lstChannel,
	}
	jsonStr, err := json.Marshal(message)
	if err != nil {
		log.Fatalln(err)
		return nil
	}
	// Create new post request
	req, err := http.NewRequest("POST", os.Getenv("JAVA_URL_API_SYNC_MESSAGES_BY_CHANNEL"), bytes.NewBuffer(jsonStr))
	if err != nil {
		log.Println(err)
		return nil
	}

	// Header - API get user information
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", a.token)

	client := &http.Client{}
	resp, err := client.Do(req)
	log.Println(resp)
	if err != nil {
		log.Println(err)
	}
	defer resp.Body.Close()

	// Parse body
	var result map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&result)

	res := result["object"].(map[string]interface{})

	return res["data"].([]interface{})
}

// SyncMessageByUser - x
func (a *API) SyncMessageByUser() []interface{} {
	// Call Java API at Url
	jsonStr := []byte(`{}`)

	// Create new post request
	req, err := http.NewRequest("POST", os.Getenv("JAVA_URL_API_SYNC_MESSAGE_BY_USER"), bytes.NewBuffer(jsonStr))
	//
	if err != nil {
		log.Println(err)
		return nil
	}
	// Header - API get user information
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", a.token)

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		log.Println(err)
		return nil
	}
	defer resp.Body.Close()

	// Parse body
	var result map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&result)

	res := result["object"].(map[string]interface{})

	return res["data"].([]interface{})
}

// GetClientsInChannel - x
func (a *API) GetClientsInChannel(name string) []string {
	// Call Java API at Url
	message := map[string]interface{}{
		"channelName": name,
	}
	jsonStr, err := json.Marshal(message)
	if err != nil {
		log.Fatalln(err)
		return nil
	}
	// Create new post request
	req, err := http.NewRequest("POST", os.Getenv("JAVA_URL_API_GET_CLIENTS_IN_CHANNEL"), bytes.NewBuffer(jsonStr))
	//
	if err != nil {
		log.Println(err)
		return nil
	}
	// Header - API get user information
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", a.token)

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		log.Println(err)
		return nil
	}
	defer resp.Body.Close()

	var result map[string]interface{}

	json.NewDecoder(resp.Body).Decode(&result)

	obj := result["object"]
	if obj == nil {
		return nil
	}
	res := obj.([]interface{})
	lstClients := make([]string, len(res))

	for i := range res {
		temp := res[i].(float64)
		lstClients[i] = strconv.Itoa(int(temp))
	}
	return lstClients
}

// DeleteMessages -x
func (a *API) DeleteMessages(lstMessages []string) {
	// log.Println("WILL DELETE THESE MESSAGE:")
	// log.Println(lstMessages)

	message := map[string]interface{}{
		"messageIds": lstMessages,
	}
	jsonStr, err := json.Marshal(message)
	if err != nil {
		log.Fatalln(err)
	}
	// Create new post request
	req, err := http.NewRequest("POST", os.Getenv("JAVA_URL_API_DELETE_ACK_MESSAGE"), bytes.NewBuffer(jsonStr))
	//log.Println(req)
	if err != nil {
		log.Println(err)
	}

	// Header - API get user information
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", a.token)

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil || resp == nil {
		log.Println(err)
		return
	}
	defer resp.Body.Close()
}
