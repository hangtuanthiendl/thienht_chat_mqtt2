package storage

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/hazelcast/hazelcast-go-client"
	"github.com/hazelcast/hazelcast-go-client/core"
)

// Storage -x
type Storage struct {
	Client       hazelcast.Client
	CacheChannel core.MultiMap
	LockClients  core.Map
}

// New - x
func New() *Storage {
	config := hazelcast.NewConfig()
	config.GroupConfig().SetName(os.Getenv("HAZELCAST_USER"))
	networkConfig := config.NetworkConfig()
	networkConfig.SetConnectionAttemptPeriod(10 * time.Second)
	networkConfig.AddAddress(os.Getenv("HAZELCAST_CLUSTER"))

	client, err := hazelcast.NewClientWithConfig(config)
	if err != nil {
		log.Panicln(err)
		return nil
	}

	cacheChannel, err := client.GetMultiMap(os.Getenv("HAZELCAST_CLIENT_WS"))
	//cacheChannel.Clear()
	if err != nil {
		log.Panic(err)
	}

	lockClients, err := client.GetMap(os.Getenv("HAZELCAST_CLIENT_LOCKED"))
	//cacheChannel.Clear()
	if err != nil {
		log.Panic(err)
	}

	return &Storage{
		Client:       client,
		CacheChannel: cacheChannel,
		LockClients:  lockClients,
	}
}

// GetMap - x
func (s *Storage) GetMap(partionID int) core.Map {
	mp, err := s.Client.GetMap(os.Getenv("HAZELCAST_NAME_MAP") + fmt.Sprint(partionID))
	//log.Println(os.Getenv("HAZELCAST_NAME_MAP") + fmt.Sprint(partionID))
	if err != nil {
		log.Panic(err)
	}
	return mp
}

// GetMultiMap - X
func (s *Storage) GetMultiMap() core.MultiMap {
	mp, err := s.Client.GetMultiMap(os.Getenv("HAZELCAST_CLIENT_WS"))
	if err != nil {
		log.Panic(err)
	}
	return mp
}

// LockClient -X
func (s *Storage) LockClient(id string) {
	s.LockClients.PutTransient(id, os.Getenv("SERVER_NAME"), 10*time.Second)
}

//CheckLock -X
func (s *Storage) CheckLock(id string) bool {
	value, _ := s.LockClients.Get(id)
	log.Println("SESSION IS RUNNING ON: ")
	log.Println(value)
	if value != nil && value != os.Getenv("SERVER_NAME") {
		return true
	}
	return false
}

// UnLockClient -X
func (s *Storage) UnLockClient(id string) {
	s.LockClients.Remove(id)
}

// Close -X
func (s *Storage) Close() {
	s.Client.Shutdown()
}
