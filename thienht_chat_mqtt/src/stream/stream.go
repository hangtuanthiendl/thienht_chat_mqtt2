package stream

import (
	"errors"
	"log"
	"os"
	"time"

	"github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
)

const (
	natsMaxReconnect        = -1   //The numbers of reconnection forever
	natsRecconectWait       = 1200 // Time wait for the next connection by seconds
	natsDrainTimeOut        = 120
	natsPingInterval        = 5000 // TODO: Change 2 param can effect buffer size
	natsMaxPingsOutstanding = 120

	defaultChannel = 0 // Type of default subscribe
	durableChannel = 1 // Type of durable subscribe
)

// SubscribeType - Type of Channel for subscrible
type SubscribeType struct {
	Type      int                `json:"type"`
	Subscribe *stan.Subscription `json:"subscribe"`
}

// NatStreaming - Carry nats connection, nats streaming connection and nats streaming client ID
type NatStreaming struct {
	natsConn *nats.Conn
	// The nats streaming connection
	StanConn stan.Conn
	// list of topics
	LstChannels map[string]*SubscribeType
}

func newSubscribe(stype int, sub *stan.Subscription) *SubscribeType {
	return &SubscribeType{
		Type:      stype,
		Subscribe: sub,
	}
}

// New - Create new connections
func New(id string) *NatStreaming {
	nc, err := nats.Connect(
		os.Getenv("NATS_CLUSTERS"),
		//nats.Name("API - CHAT BY GOLANG"),
		//nats.NoEcho(),
		nats.DrainTimeout(natsDrainTimeOut*time.Second),
		//nats.MaxReconnects(nats.ReconnectForever),
		nats.ReconnectWait(natsRecconectWait*time.Second),
		nats.UserInfo(os.Getenv("NATS_USER"), os.Getenv("NATS_PASSWORD")),
		nats.PingInterval(natsPingInterval*time.Second),
		nats.MaxPingsOutstanding(natsMaxPingsOutstanding),
		nats.ReconnectBufSize(5000*1024*1024),
		nats.DisconnectErrHandler(func(nc *nats.Conn, err error) {
			log.Println("Got disconnected! Reason: ")
			log.Println(err)
		}),
		nats.ReconnectHandler(func(nc *nats.Conn) {
			log.Println("Got reconnected to:")
			log.Println(nc.ConnectedUrl())
		}),
		nats.ClosedHandler(func(nc *nats.Conn) {
			log.Println("Connection closed. Reason")
			log.Println(nc.LastError())
		}),
		nats.ErrorHandler(func(_ *nats.Conn, _ *nats.Subscription, err error) {
			log.Printf("Error: %v", err)
		}),
		nats.ErrorHandler(func(_ *nats.Conn, _ *nats.Subscription, err error) {
			log.Println(err)
		}),
	)

	if err != nil {
		log.Println("NATS ERROR! >>")
		log.Println(err)
		return nil
	}

	//Connect to nats streaming
	sc, err := stan.Connect(
		os.Getenv("NATS_STREAMING_CLUSTER_ID"),
		os.Getenv("NATS_CLIENT")+id,
		stan.MaxPubAcksInflight(100),
		//stan.MaxInflight(100),
		stan.Pings(natsPingInterval, natsMaxPingsOutstanding),
		stan.SetConnectionLostHandler(func(_ stan.Conn, reason error) {
			log.Println(reason)
		}),

		stan.NatsConn(nc))

	if err != nil {
		log.Println("NATS STREAM ERROR!")
		log.Println(err)
		return nil
	}
	return &NatStreaming{
		natsConn:    nc,
		StanConn:    sc,
		LstChannels: make(map[string]*SubscribeType),
	}
}

// Subscribe - Subscrible channel //TODO: Du thua code
func (n *NatStreaming) SubscribeChannel(channelType int, subject string, f func(uint64, []byte)) *stan.Subscription {
	switch channelType {
	case durableChannel:
		s, err := n.StanConn.Subscribe(subject, func(m *stan.Msg) {
			f(m.Sequence, m.Data)
		}, stan.DurableName(subject))
		if err != nil {
			log.Println(err)
			return nil
		}
		n.LstChannels[subject] = newSubscribe(durableChannel, &s)
		return &s
	default:
		s, err := n.StanConn.Subscribe(subject, func(m *stan.Msg) {
			f(m.Sequence, m.Data)
		})
		if err != nil {
			log.Println(err)
			return nil
		}
		return &s
	}
}

// SubscribleChannels -
func (n *NatStreaming) SubscribleChannels(out chan []byte, channelType int, lstChannels []string) error {
	if n.StanConn == nil {
		log.Panicln("Can not connect to nats streaming")
		return errors.New("Can not connect to nats streaming")
	}
	for _, subject := range lstChannels {
		if n.LstChannels[subject] == nil {
			log.Println("[Infor] ->>>>>>> Actived channel: " + subject + "...")
			var sub *stan.Subscription

			cb := func(seq uint64, data []byte) {
				out <- data
			}

			var data *SubscribeType = nil
			switch channelType {
			case durableChannel:
				sub = n.SubscribeChannel(durableChannel, subject, cb)
				data = &SubscribeType{
					Type:      durableChannel,
					Subscribe: sub,
				}
			default:
				data = &SubscribeType{
					Type:      defaultChannel,
					Subscribe: sub,
				}
				sub = n.SubscribeChannel(defaultChannel, subject, cb)
			}

			n.LstChannels[subject] = data
		}
	}
	return nil
}

// UnSubscribeChannels - Unsubscrible channel, remove channel from list
func (n *NatStreaming) UnSubscribeChannels(lstChannels []string) error {
	if n.StanConn == nil {
		log.Panicln("Can not connect to nats streaming")
		return errors.New("Can not connect to nats streaming")
	}
	for _, subject := range lstChannels {
		sub := n.LstChannels[subject]
		if sub != nil { //TODO: Khong can kiem tra, nats co tra ve loi khi unsub hai lan khong
			data := *sub.Subscribe
			sub := data.(stan.Subscription)
			if err := sub.Unsubscribe(); err != nil {
				log.Println(err)
			}
			delete(n.LstChannels, subject)
		}
	}
	return nil
}

// Send publishes new message
func (n *NatStreaming) Send(id string, msg []byte, f func(ackedNuid string, err error)) {
	if n.StanConn == nil {
		f("", errors.New("Can not connect to nats streaming"))
	}
	n.StanConn.PublishAsync(id, msg, f)
}

// Close - Close nats connection
func (n *NatStreaming) Close() {
	for channel := range n.LstChannels {
		if n.LstChannels[channel].Type == defaultChannel {
			data := (*n.LstChannels[channel].Subscribe).(stan.Subscription)
			data.Unsubscribe()
		}
	}
	if n.StanConn != nil {
		n.StanConn.Close()
	}
	// if n.natsConn != nil {
	// 	n.natsConn.Close()
	// }
}
