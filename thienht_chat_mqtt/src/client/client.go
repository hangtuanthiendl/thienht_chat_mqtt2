package client

import (
	apisession "chat/src/api"
	message "chat/src/message"
	stream "chat/src/stream"
	socket "chat/src/websocket"
	"container/list"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"
)

// --------------------------------- NATS + Java -----------------------------------------------
// -----------------------------------/------\--------------------------------------------------
// ---------------------------------Go-------Go-------------------------------------------------
// ---------------------------------/----------\------------------------------------------------
// ------------------------------ClientA-------ClientB------------------------------------------

const (
	// Option when subscrible channel
	defaultChannel = 0 // Type of default subscribe
	durableChannel = 1 // Type of durable subscribe

	// Sync Message
	syncQueue   = 1 //Message from queue of each client
	syncHistory = 2 //Message from history

	flushTime = 10 * time.Second
	maxMsg    = 3
)

// Client - Each clientID will have many clients (e.g: access with multiple tabs), but they carries only one nats-streaming connection
type Client struct {
	// Hub
	hub *Hub
	// The websocket connection.
	Websocket *list.List //TODO: slice or list which is better
	// clientID for nats streaming (unique)
	clientID string
	// partionID on Hazelcast
	partionID int
	// api session
	api *apisession.API
	// The nats connection
	stream *stream.NatStreaming

	// Buffered channel of inbound messages
	ReceiveQueue chan []byte

	//
	NatQueueIn  chan *message.Message
	NatQueueOut chan []byte

	//
	ListAck []string
}

// New - Create a new Client
func New(hub *Hub, ws *socket.Socket, stream *stream.NatStreaming, clientID string, partionID int, apiss *apisession.API) *Client {
	lstws := list.New()
	lstws.PushBack(ws)
	return &Client{
		hub:       hub,
		Websocket: lstws,
		clientID:  clientID,
		partionID: partionID,
		api:       apiss,
		stream:    stream,

		ReceiveQueue: make(chan []byte),

		NatQueueIn:  make(chan *message.Message),
		NatQueueOut: make(chan []byte),

		ListAck: []string{},
	}
}

// Control - Filter message with type message
func (c *Client) Control(msg *message.Message) {
	defer func() {
		//.hub.Unregister <- c
	}()
	// Excuting message base on its type
	if msg != nil {
		switch msg.Type {
		case "text", "img", "videos", "emo": // Send message to  & Nats Streaming
			if c.stream.LstChannels[msg.Destination] != nil {
				go c.PutKafkaNat(msg)
				go c.PushMessageToHazelcast(msg)
			} else {
				c.SendMessages(message.NewErrorMessage(msg.ID, message.ErrChannel))
			}
		case "register": // Create a new channel
			var iClients []int
			// Covert string to int array
			err := json.Unmarshal([]byte(*msg.Payload), &iClients)
			if err != nil {
				log.Println(err)
			}
			// Create new channel or find channel if exist
			channelName, err := c.api.CreateChannel(iClients)
			if err != nil {
				c.SendMessages(message.NewErrorMessage(msg.ID, message.ErrChannel))
				return
			}
			log.Println(channelName)
			// Send channel name to all clients belong to channel
			msg.Source = channelName
			msg.Ack = message.AckReceived
			err2 := c.stream.SubscribleChannels(c.NatQueueOut, durableChannel, []string{channelName})
			if err2 != nil {
				c.SendMessages(message.NewErrorMessage(msg.ID, message.ErrNats))
			}
			// Send created successfully message to all clients
			for _, value := range iClients {
				msg.Destination = os.Getenv("NATS_DEFAULT_CHANNEL") + fmt.Sprint(value)
				c.NatQueueIn <- msg
			}
		case "sync", "subscrible", "unsubscrible": // Sync all message from java
			var lstChannels []string
			// Covert string to int array
			err := json.Unmarshal([]byte(*msg.Payload), &lstChannels)
			if err != nil {
				log.Panic(err)
			}
			if msg.Type == "sync" {
				c.SyncMessageByChannel(lstChannels, true)
			}
			if msg.Type == "subscrible" {
				err := c.stream.SubscribleChannels(c.NatQueueOut, defaultChannel, lstChannels)
				if err != nil {
					c.SendMessages(message.NewErrorMessage(msg.ID, message.ErrNats))
				}
			}
			if msg.Type == "unsubscrible" {
				err := c.stream.UnSubscribeChannels(lstChannels)
				if err != nil {
					c.SendMessages(message.NewErrorMessage(msg.ID, message.ErrNats))
				}
			}
		default: //ack
			c.NatQueueIn <- msg
		}
	}
}

// ControlWsMessage - Handling message
func (c *Client) ControlWsMessage() {
	ticker := time.NewTicker(flushTime)

	defer func() {
		//c.hub.Unregister <- c
		ticker.Stop()
	}()

	for {
		select {
		case data, ok := <-c.ReceiveQueue:
			if !ok {
				return
			}
			// Validate Message when parsing from byte array
			msg, id, errStatus := message.NewMessage(data)
			if errStatus != 0 {
				c.SendMessages(message.NewErrorMessage(id, errStatus))
				return
			}
			if msg != nil {
				msg.Ack = message.AckSent
				c.SendMessages(msg)
				c.Control(msg)
			}
		case <-ticker.C:
			c.hub.Hstorage.LockClient(c.clientID)
			//log.Println(c.ListAck)
			if len(c.ListAck) > 0 {
				c.api.DeleteMessages(c.ListAck)
				c.ListAck = []string{}
			}
		}

	}
}

// ControlStreamMessage - X
func (c *Client) ControlStreamMessage() {
	defer func() {
		c.hub.Unregister <- c
	}()
	for {
		select {
		case msg := <-c.NatQueueIn:
			msg.Ack = message.AckReceived
			data, _ := message.ConvertMessageToByte(msg)
			cbHandeler := func(ackedNuid string, err error) {
				if err != nil {
					log.Println(ackedNuid)
					res := message.NewErrorMessage(msg.ID, message.ErrQueue)
					log.Println(res)
					c.SendMessages(res)
					return
				} else {
					c.SendMessages(msg)
				}
			}
			c.stream.Send(msg.Destination, data, cbHandeler)
		case data := <-c.NatQueueOut:
			// Parse message from byte to Message
			msg := message.ConvertByteToMessage(data)
			log.Println(msg)
			if msg == nil {
				log.Println("ERROR: Wrong message from nats")
				return
			}
			msg.Ack = message.AckDeliveried
			if msg.Type == "ack" {
				c.RemoveMessageFromHazelcast(msg) //TODO REMOVE LIIST
				c.ListAck = append(c.ListAck, msg.Source+"_"+msg.ID)
				//log.Println(c.ListAck)
				if len(c.ListAck) > maxMsg {
					c.api.DeleteMessages(c.ListAck)
					c.ListAck = []string{}
				}
			}

			if msg.Type == "register" {
				err := c.stream.SubscribleChannels(c.NatQueueOut, durableChannel, []string{msg.Source}) //TODO
				if err != nil {
					c.SendMessages(message.NewErrorMessage(msg.ID, message.ErrNats))
				}
			}
			c.SendMessages(msg)
		}
	}
}

// SendMessages - Receive message from nats - ACK 3 and send message to websockets
func (c *Client) SendMessages(msg interface{}) {
	for element := c.Websocket.Front(); element != nil; element = element.Next() {
		element.Value.(*socket.Socket).SendQueue <- msg
	}
}

// PutKafkaNat - X
func (c *Client) PutKafkaNat(msg *message.Message) {
	err := c.hub.Kafkaq.PushMessageToKafkaHistory(msg)
	if err != nil {
		res := message.NewErrorMessage(msg.ID, message.ErrKafka)
		c.SendMessages(res)
	} else {
		c.NatQueueIn <- msg
	}
}

// PushMessageToHazelcast - xxxxxxxxxxxxxxxxxxxxxx
func (c *Client) PushMessageToHazelcast(msg *message.Message) {
	var lstClients []string = nil
	if c.stream.LstChannels[msg.Destination] != nil {
		value, err := c.hub.Hstorage.CacheChannel.Get(msg.Destination)
		if err == nil || len(value) > 0 {
			for _, v := range value {
				lstClients = append(lstClients, v.(string))
			}
		}
	}

	if len(lstClients) == 0 {
		lstClients = c.api.GetClientsInChannel(msg.Destination)
		//log.Println(lstClients)
		for _, value := range lstClients {
			c.hub.Hstorage.CacheChannel.Put(msg.Destination, fmt.Sprint(value))
		}
	}

	for _, client := range lstClients {
		if c.clientID != client {
			clientID := client + "_" + msg.ID
			value, _ := message.ConvertMessageToByte(msg)
			c.hub.Hstorage.GetMap(c.partionID).PutTransient(clientID, value, 3*time.Second)
		}
	}
}

// RemoveMessageFromHazelcast - x
func (c *Client) RemoveMessageFromHazelcast(msg *message.Message) {
	clientID := msg.Source + "_" + msg.ID
	c.hub.Hstorage.GetMap(c.partionID).Remove(clientID)
}

// SyncMessageByUser - Pull messages from history
func (c *Client) SyncMessageByUser() {
	messages := c.api.SyncMessageByUser()
	if messages != nil {
		for _, msg := range messages {
			c.SendMessages(msg)
		}
	}
}

// SyncMessageByChannel - Pull messages from queue
func (c *Client) SyncMessageByChannel(lstChannels []string, condition bool) {
	if condition == true && len(lstChannels) > 0 {
		messages := c.api.SyncMessageByChannel(lstChannels)
		if messages != nil {
			for _, msg := range messages {
				c.SendMessages(msg)
			}
		}
	}
}
