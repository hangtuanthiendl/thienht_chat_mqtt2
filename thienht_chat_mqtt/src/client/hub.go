package client

import (
	queue "chat/src/queue"
	"chat/src/storage"
	"log"
)

// Hub roles as a bridge, it registers a new Client to map and removes it if Client disconnects to sever
type Hub struct {
	// Registered clients.
	Clients map[string]*Client
	// Register requests from the clients.
	Register chan *Client
	// Unregister requests from clients.
	Unregister chan *Client
	// Kafka queue
	Kafkaq *queue.KafkaQueue
	//
	Hstorage *storage.Storage
}

// NewHub - Create a new Hub
func NewHub() *Hub {
	hstorage := storage.New()
	return &Hub{
		Register:   make(chan *Client),
		Unregister: make(chan *Client),
		Clients:    make(map[string]*Client, 1000000), //TODO
		Kafkaq:     queue.New(),
		Hstorage:   hstorage,
	}
}

// Run - Add or remove client from list
func (h *Hub) Run() {
	for {
		select {
		case client := <-h.Register:
			if h.Clients[client.clientID] == nil {
				h.Clients[client.clientID] = client
			}
		case client := <-h.Unregister:
			if client.Websocket.Len() == 0 {
				client.stream.Close()
				log.Println("Destroy! " + client.clientID)
				h.Hstorage.UnLockClient(client.clientID)

			}
			delete(h.Clients, client.clientID)
		}
	}
}
