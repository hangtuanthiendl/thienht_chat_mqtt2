package models

import (
	"encoding/json"
	"fmt"
	"log"
	"reflect"
)

// Ack Message
const (
	AckSent       = 1 // Messages sent from A, arrived to Go
	AckReceived   = 2 // Messages sent from Go, arrived to Nats and Kafka
	AckDeliveried = 3 // Messages received from Go, are sending to B
	AckSeen       = 4 // B recceived message
)

var (
	//ErrWrongID - ID's Message has a wrong format
	ErrWrongID = 1
	//ErrWrongType - ID's Message has a wrong format
	ErrWrongType = 2
	//ErrWrongDestination - ID's Message has a wrong format
	ErrWrongDestination = 3
	//ErrWrongSource - ID's Message has a wrong format
	ErrWrongSource = 4
	//ErrWrongPayload - ID's Message has a wrong format
	ErrWrongPayload = 5
	//ErrInternalServer - ID's Message has a wrong format
	ErrInternalServer = 6
	//ErrAbnormal - ID's Message has a wrong format
	ErrAbnormal = 7
	//ErrAPI - ID's Message has a wrong format
	ErrAPI = 8
	//ErrKafka - ID's Message has a wrong format
	ErrKafka = 9
	//ErrNats - ID's Message has a wrong format
	ErrNats = 10
	//
	ErrLocked = 11
	//
	ErrLoginFailed = 12
	//
	ErrChannel = 14
	//
	ErrQueue = 15
)

// BaseMessage - Interface of abstract message
type BaseMessage interface {
}

// Message - Message which is used at receive chat message
type Message struct {
	ID          string           `json:"id"`
	Type        string           `json:"type"`
	Destination string           `json:"destination"` // Recevier
	Source      string           `json:"source"`      // Sender
	Payload     *json.RawMessage `json:"payload"`     // Message contents
	Ack         int              `json:"ack"`         // ACK: 1-> ws, 2 -> nats, 3 -> go, 4-> client

}

// ErrorMessage - Handing error message
type ErrorMessage struct {
	ID        string `json:"id"`
	Type      string `json:"type"`
	ErrCode   string `json:"errorType"`
	ErrReason string `json:"errorReason"`
}

// NewMessage -
func NewMessage(data []byte) (*Message, string, int) {
	// Map data from client
	m := make(map[string]interface{})
	err := json.Unmarshal(data, &m)
	if err != nil {
		return nil, "", ErrAbnormal
	}

	mID := m["id"]
	mType := m["type"]
	mSource := m["source"]
	mDestination := m["destination"]
	mPayload := m["payload"]

	// Validate Message type
	res := Message{}
	// ID of message
	if mID == nil || reflect.TypeOf(mID).Kind() != reflect.String {
		return nil, "", ErrWrongID
	}
	res.ID = mID.(string)

	// Kind of message
	if reflect.TypeOf(mType).Kind() != reflect.String || mType == nil {
		return nil, "", ErrWrongType
	}
	res.Type = m["type"].(string)

	// Source
	if mSource == nil || reflect.TypeOf(mSource).Kind() != reflect.String {
		return nil, res.ID, ErrWrongSource
	}
	res.Source = mSource.(string)

	// Destination
	if mDestination == nil || reflect.TypeOf(mDestination).Kind() != reflect.String {
		return nil, res.ID, ErrWrongDestination
	}
	res.Destination = mDestination.(string)

	switch mType {
	case "ack":
		temp := json.RawMessage("{}")
		res.Payload = &temp
	case "register":
		// Payload
		if mPayload == nil || reflect.TypeOf(mPayload).Kind() != reflect.Slice {
			return nil, "", ErrWrongPayload
		}

		temp := mPayload.([]interface{})
		b, err := json.Marshal(temp)
		if err != nil {
			return nil, res.ID, ErrAbnormal
		}
		for _, v := range temp {
			if v == nil || reflect.TypeOf(v).Kind() != reflect.Float64 {
				return nil, res.ID, ErrWrongPayload
			}
		}
		data := json.RawMessage(b)
		res.Payload = &data
	case "sync", "subscrible", "unsubscrible":
		// Payload
		if mPayload == nil || reflect.TypeOf(mPayload).Kind() != reflect.Slice {
			return nil, res.ID, ErrWrongPayload
		}

		temp := mPayload.([]interface{})
		b, err := json.Marshal(temp)
		if err != nil {
			return nil, res.ID, ErrAbnormal
		}
		for _, v := range temp {
			if v == nil || reflect.TypeOf(v).Kind() != reflect.String {
				return nil, res.ID, ErrWrongPayload
			}
		}
		data := json.RawMessage(b)
		res.Payload = &data
	default: // Type = "text" , "img", "videos", "emo",
		// Payload must be string
		if mPayload == nil || reflect.TypeOf(mPayload).Kind() != reflect.String {
			return nil, res.ID, ErrWrongPayload
		}
		temp := mPayload.(string)
		b, err := json.Marshal(temp)
		if err != nil {
			return nil, res.ID, ErrWrongPayload
		}
		data := json.RawMessage(b)
		res.Payload = &data
	}

	return &res, res.ID, 0
}

// NewErrorMessage -
func NewErrorMessage(id string, messageType int) *ErrorMessage {
	res := ErrorMessage{
		ID: id,
	}
	switch messageType {
	// Validate message error
	case ErrWrongID:
		res.Type = "ERROR"
		res.ErrCode = "E001"
		res.ErrReason = "Wrong type of field: id"
	case ErrWrongType:
		res.Type = "ERROR"
		res.ErrCode = "E002"
		res.ErrReason = "Wrong type of field: type"
	case ErrWrongDestination:
		res.Type = "ERROR"
		res.ErrCode = "E003"
		res.ErrReason = "Wrong type of field: destination"
	case ErrWrongSource:
		res.Type = "ERROR"
		res.ErrCode = "E004"
		res.ErrReason = "Wrong type of field: source"
	case ErrWrongPayload:
		res.Type = "ERROR"
		res.ErrCode = "E005"
		res.ErrReason = "Wrong type of field: payload"
	case ErrAbnormal:
		res.Type = "ERROR"
		res.ErrCode = "E006"
		res.ErrReason = "Abnormal error"
	case ErrInternalServer:
		res.Type = "ERROR"
		res.ErrCode = "E007"
		res.ErrReason = "Internal Server Error"
	case ErrAPI:
		res.Type = "ERROR"
		res.ErrCode = "E008"
		res.ErrReason = "API can not access"
	case ErrKafka:
		res.Type = "ERROR"
		res.ErrCode = "E009"
		res.ErrReason = "Kafka server is not working right"
	case ErrNats:
		res.Type = "ERROR"
		res.ErrCode = "E010"
		res.ErrReason = "Nats server is not working right"
	case ErrLocked:
		res.Type = "ERROR"
		res.ErrCode = "E011"
		res.ErrReason = "Client Locked"
	case ErrLoginFailed:
		res.Type = "ERROR"
		res.ErrCode = "E012"
		res.ErrReason = "Login Failed - Wrong token!"
	case ErrChannel:
		res.Type = "ERROR"
		res.ErrCode = "E014"
		res.ErrReason = "Messages Failed - Wrong channel!"
	case ErrQueue:
		res.Type = "ERROR"
		res.ErrCode = "E015"
		res.ErrReason = "Queue overload. Please wait after 10 seconds!"
	default:
		res.Type = "ERROR"
		res.ErrCode = "E000"
		res.ErrReason = "Server is not working right"
	}
	return &res
}

// ConvertByteToMessage - Transform type of data from byte to Message
func ConvertByteToMessage(msg []byte) *Message {
	message := Message{}
	err := json.Unmarshal(msg, &message)
	if err != nil {
		log.Println(err)
		return nil
	}
	return &message
}

// ConvertMessageToByte -X
func ConvertMessageToByte(msg interface{}) ([]byte, error) {
	data, err := json.Marshal(msg)
	if err != nil {
		log.Panic(err)
		return nil, err
	}
	return data, nil
}

//TODO
func dump_interface_array(args interface{}) {
	val := reflect.ValueOf(args)
	fmt.Println(val.Kind())
	if val.Kind() == reflect.Array {
		fmt.Println("len = ", val.Len())
		for i := 0; i < val.Len(); i++ {
			e := val.Index(i)
			switch e.Kind() {
			case reflect.Int:
				fmt.Printf("%v, ", e.Int())
			case reflect.Float32:
				fallthrough
			case reflect.Float64:
				fmt.Printf("%v, ", e.Float())
			default:
				panic(fmt.Sprintf("invalid Kind: %v", e.Kind()))
			}
		}
	}
}

//TODO: SUA cac ham convert this.
