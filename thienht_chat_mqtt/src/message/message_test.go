package models

import (
	"encoding/json"
	"testing"
)

func TestNewMessage(t *testing.T) {
	payloadText := json.RawMessage(`{"foo":"bar"}`)
	//Check Wrong type of format
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//|	ID	|	ID: string		| Type: string		| Destination: string		| Source: string		| Source: json.RawMessage		| Ack: int		|			RESULT		 		|
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//|	T1	|		int			|					|							|						|								|				|								|
	//|	T2	|					|		int			|							|						|								|				|								|
	//|	T3	|					|					|			int				|						|								|				|								|
	//|	T4	|					|					|							|			int			|								|				|								|
	//|		|					|					|							|						|								|				|								|
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	checkID := []struct {
		ID          int              `json:"id"`
		Type        string           `json:"type"`
		Destination string           `json:"destination"` // Recevier
		Source      string           `json:"source"`      // Sender
		Payload     *json.RawMessage `json:"payload"`     // Message contents
	}{
		{
			ID:          1,
			Type:        "",
			Destination: "",
			Source:      "",
			Payload:     &payloadText,
		},
	}
	for _, tt := range checkID {
		t.Run("Check Wrong type format", func(t *testing.T) {
			b, _ := json.Marshal(tt)
			msg, _, i := NewMessage(b)
			if i != ErrWrongID && msg != nil {
				t.Errorf("Must recheck id format")
			}
		})
	}

	checkType := []struct {
		ID          string           `json:"id"`
		Type        int              `json:"type"`
		Destination string           `json:"destination"` // Recevier
		Source      string           `json:"source"`      // Sender
		Payload     *json.RawMessage `json:"payload"`     // Message contents
	}{
		{
			ID:          "",
			Type:        1,
			Destination: "",
			Source:      "",
			Payload:     &payloadText,
		},
	}
	for _, tt := range checkType {
		t.Run("Check Wrong type format", func(t *testing.T) {
			b, _ := json.Marshal(tt)
			msg, _, i := NewMessage(b)
			if i != ErrWrongType && msg != nil {
				t.Errorf("Must recheck type format")
			}
		})
	}

	checkDestination := []struct {
		ID          string           `json:"id"`
		Type        string           `json:"type"`
		Destination int              `json:"destination"` // Recevier
		Source      string           `json:"source"`      // Sender
		Payload     *json.RawMessage `json:"payload"`     // Message contents
	}{
		{
			ID:          "",
			Type:        "",
			Destination: 1,
			Source:      "",
			Payload:     &payloadText,
		},
	}
	for _, tt := range checkDestination {
		t.Run("Check Wrong type format", func(t *testing.T) {
			b, _ := json.Marshal(tt)
			msg, _, i := NewMessage(b)
			if i != ErrWrongDestination && msg != nil {
				t.Errorf("Must recheck destination format")
			}
		})
	}

	checkSource := []struct {
		ID          string           `json:"id"`
		Type        string           `json:"type"`
		Destination string           `json:"destination"` // Recevier
		Source      int              `json:"source"`      // Sender
		Payload     *json.RawMessage `json:"payload"`     // Message contents
	}{
		{
			ID:          "",
			Type:        "",
			Destination: "",
			Source:      1,
			Payload:     &payloadText,
		},
	}
	for _, tt := range checkSource {
		t.Run("Check Wrong type format", func(t *testing.T) {
			b, _ := json.Marshal(tt)
			msg, _, i := NewMessage(b)
			if i != ErrWrongDestination && msg != nil {
				t.Errorf("Must recheck source format")
			}
		})
	}
	//Check Payload
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//|	ID	|	ID: string		| Type: string		| Destination: string		| Source: string		| Payload: json.RawMessage		| Ack: int		|			RESULT		 		|
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//|	P1	|		"P1"		|		text		|		"1"					|		"2"				|			payloadText			|				|		OK						|
	//|	P2	|		"P2"		|		text		|		"1"					|		"2"				|			nil					|				|		ErrWrongPayload			|
	//|	P3	|		"P3"		|		text		|		"1"					|		"2"				|			payloadArrayInt		|				|		ErrWrongPayload			|
	//|	P4	|		"P4"		|		text		|		"1"					|		"2"				|			payloadArrayString	|				|		ErrWrongPayload			|
	//|	P5	|		"P5"		|		text		|		"1"					|		"2"				|			payloadFloat		|				|		ErrWrongPayload			|
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
	//|	P1	|		"P6"		|		ack			|		"1"					|		"2"				|			payloadText			|				|		OK						|
	//|	P2	|		"P7"		|		ack			|		"1"					|		"2"				|			nil					|				|		OK						|
	//|	P3	|		"P8"		|		ack			|		"1"					|		"2"				|			payloadArrayInt		|				|		OK						|
	//|	P4	|		"P9"		|		ack			|		"1"					|		"2"				|			payloadArrayString	|				|		OK						|
	//|	P5	|		"P10"		|		ack			|		"1"					|		"2"				|			payloadFloat		|				|		OK						|
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
	//|	P1	|		"P11"		|		sync		|		"1"					|		"2"				|			payloadText			|				|		ErrWrongPayload			|
	//|	P2	|		"P12"		|		sync		|		"1"					|		"2"				|			nil					|				|		ErrWrongPayload			|
	//|	P3	|		"P13"		|		sync		|		"1"					|		"2"				|			payloadArrayInt		|				|		ErrWrongPayload			|
	//|	P4	|		"P14"		|		sync		|		"1"					|		"2"				|			payloadArrayString	|				|		OK						|
	//|	P5	|		"P15"		|		sync		|		"1"					|		"2"				|			payloadFloat		|				|		ErrWrongPayload			|
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|					|					|							|						|								|				|								|
	//|	P1	|		"P16"		|		register	|		"1"					|		"2"				|			payloadText			|				|		ErrWrongPayload			|
	//|	P2	|		"P17"		|		register	|		"1"					|		"2"				|			nil					|				|		ErrWrongPayload			|
	//|	P3	|		"P18"		|		register	|		"1"					|		"2"				|			payloadArrayInt		|				|		OK						|
	//|	P4	|		"P19"		|		register	|		"1"					|		"2"				|			payloadArrayString	|				|		ErrWrongPayload			|
	//|	P5	|		"P20"		|		register	|		"1"					|		"2"				|			payloadFloat		|				|		ErrWrongPayload			|
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|					|						|								|				|								|
	//|	P1	|		"P21"		|		subscrible	|		"1"					|		"2"				|			payloadText			|				|		ErrWrongPayload			|
	//|	P2	|		"P22"		|		subscrible	|		"1"					|		"2"				|			nil					|				|		ErrWrongPayload			|
	//|	P3	|		"P23"		|		subscrible	|		"1"					|		"2"				|			payloadArrayInt		|				|		ErrWrongPayload			|
	//|	P4	|		"P24"		|		subscrible	|		"1"					|		"2"				|			payloadArrayString	|				|		OK						|
	//|	P5	|		"P25"		|		subscrible	|		"1"					|		"2"				|			payloadFloat		|				|		ErrWrongPayload			|
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//|	P1	|		"P26"		|	unsubscrible	|		"1"					|		"2"				|			payloadText			|				|		ErrWrongPayload			|
	//|	P2	|		"P27"		|	unsubscrible	|		"1"					|		"2"				|			nil					|				|		ErrWrongPayload			|
	//|	P3	|		"P28"		|	unsubscrible	|		"1"					|		"2"				|			payloadArrayInt		|				|		ErrWrongPayload			|
	//|	P4	|		"P29"		|	unsubscrible	|		"1"					|		"2"				|			payloadArrayString	|				|		OK			|
	//|	P5	|		"P30"		|	unsubscrible	|		"1"					|		"2"				|			payloadFloat		|				|		ErrWrongPayload			|
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//|	P1	|		"P31"		|	unsubscribleall	|		"1"					|		"2"				|			payloadText			|				|		-						|
	//|	P2	|		"P32"		|	unsubscribleall	|		"1"					|		"2"				|			nil					|				|		-						|
	//|	P3	|		"P33"		|	unsubscribleall	|		"1"					|		"2"				|			payloadArrayInt		|				|		-						|
	//|	P4	|		"P34"		|	unsubscribleall	|		"1"					|		"2"				|			payloadArrayString	|				|		-						|
	//|	P5	|		"P35"		|	unsubscribleall	|		"1"					|		"2"				|			payloadFloat		|				|		-						|
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//|	P1	|		"P36"		|		other		|		"1"					|		"2"				|			payloadText			|				|		OK						|
	//|	P2	|		"P37"		|		other		|		"1"					|		"2"				|			nil					|				|		ErrWrongPayload			|
	//|	P3	|		"P38"		|		other		|		"1"					|		"2"				|			payloadArrayInt		|				|		ErrWrongPayload			|
	//|	P4	|		"P39"		|		otjer		|		"1"					|		"2"				|			payloadArrayString	|				|		ErrWrongPayload			|
	//|	P5	|		"P40"		|		other		|		"1"					|		"2"				|			payloadFloat		|				|		ErrWrongPayload			|
	//|		|					|					|							|						|								|				|								|
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	arrString := [2]string{"a", "b"}
	payloadArrayString, _ := json.Marshal(arrString)

	arrInt := [2]int{1, 2}
	payloadArrayInt, _ := json.Marshal(arrInt)

	arrFloat := [2]float32{1.5, 2.5}
	payloadFloat, _ := json.Marshal(arrFloat)
	checkPayload := []struct {
		ID          string           `json:"id"`
		Type        string           `json:"type"`
		Destination string           `json:"destination"` // Recevier
		Source      string           `json:"source"`      // Sender
		Payload     *json.RawMessage `json:"payload"`     // Message contents
	}{
		//type = text
		{
			ID:          "P1",
			Type:        "text",
			Destination: "1",
			Source:      "2",
			Payload:     &payloadText,
		},
		{
			ID:          "P2",
			Type:        "text",
			Destination: "1",
			Source:      "2",
			Payload:     nil,
		},
		{
			ID:          "P3",
			Type:        "text",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadArrayInt),
		},
		{
			ID:          "P4",
			Type:        "text",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadArrayString),
		},
		{
			ID:          "P5",
			Type:        "text",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadFloat),
		},

		//type =ack
		{
			ID:          "P6",
			Type:        "ack",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadText),
		},
		{
			ID:          "P7",
			Type:        "ack",
			Destination: "1",
			Source:      "2",
			Payload:     nil,
		},
		{
			ID:          "P8",
			Type:        "ack",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadArrayInt),
		},
		{
			ID:          "P9",
			Type:        "ack",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadArrayString),
		},
		{
			ID:          "P10",
			Type:        "ack",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadFloat),
		},

		//type =sync
		{
			ID:          "P11",
			Type:        "sync",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadText),
		},
		{
			ID:          "P12",
			Type:        "sync",
			Destination: "1",
			Source:      "2",
			Payload:     nil,
		},
		{
			ID:          "P13",
			Type:        "sync",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadArrayInt),
		},
		{
			ID:          "P14",
			Type:        "sync",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadArrayString),
		},
		{
			ID:          "P15",
			Type:        "sync",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadFloat),
		},

		//type =register
		{
			ID:          "P16",
			Type:        "register",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadText),
		},
		{
			ID:          "P17",
			Type:        "register",
			Destination: "1",
			Source:      "2",
			Payload:     nil,
		},
		{
			ID:          "P18",
			Type:        "register",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadArrayInt),
		},
		{
			ID:          "P19",
			Type:        "register",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadArrayString),
		},
		{
			ID:          "P20",
			Type:        "register",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadFloat),
		},

		//type =subscrible
		{
			ID:          "P21",
			Type:        "subscrible",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadText),
		},
		{
			ID:          "P22",
			Type:        "subscrible",
			Destination: "1",
			Source:      "2",
			Payload:     nil,
		},
		{
			ID:          "P23",
			Type:        "subscrible",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadArrayInt),
		},
		{
			ID:          "P24",
			Type:        "subscrible",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadArrayString),
		},
		{
			ID:          "P25",
			Type:        "subscrible",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadFloat),
		},

		//type =sync
		{
			ID:          "P26",
			Type:        "unsubscrible",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadText),
		},
		{
			ID:          "P27",
			Type:        "unsubscrible",
			Destination: "1",
			Source:      "2",
			Payload:     nil,
		},
		{
			ID:          "P28",
			Type:        "unsubscrible",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadArrayInt),
		},
		{
			ID:          "P29",
			Type:        "unsubscrible",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadArrayString),
		},
		{
			ID:          "P30",
			Type:        "unsubscrible",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadFloat),
		},

		//type =unsubscribleall
		{
			ID:          "P31",
			Type:        "unsubscribleall",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadText),
		},
		{
			ID:          "P32",
			Type:        "unsubscribleall",
			Destination: "1",
			Source:      "2",
			Payload:     nil,
		},
		{
			ID:          "P33",
			Type:        "unsubscribleall",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadArrayInt),
		},
		{
			ID:          "P34",
			Type:        "unsubscribleall",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadArrayString),
		},
		{
			ID:          "P35",
			Type:        "unsubscribleall",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadFloat),
		},

		//type =dafule
		{
			ID:          "P36",
			Type:        "other - any thing",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadText),
		},
		{
			ID:          "P37",
			Type:        "other - any thing",
			Destination: "1",
			Source:      "2",
			Payload:     nil,
		},
		{
			ID:          "P38",
			Type:        "other - any thing",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadArrayInt),
		},
		{
			ID:          "P39",
			Type:        "other - any thing",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadArrayString),
		},
		{
			ID:          "P40",
			Type:        "other - any thing",
			Destination: "1",
			Source:      "2",
			Payload:     (*json.RawMessage)(&payloadFloat),
		},
	}

	for _, tt := range checkPayload {
		t.Run(tt.ID, func(t *testing.T) {
			b, _ := json.Marshal(tt)
			msg, _, i := NewMessage(b)
			if i != ErrWrongPayload && msg == nil {
				t.Errorf("Payload wrong in id: "+tt.ID+". Error code: %d", i)
				t.Error(msg)
			}
		})
	}
}

func TestNewErrorMessage(t *testing.T) {
	//-------------------------------------------------------------------------------------------------------------
	//|	STT	|	id: string 		|		Message Type: int 						| Result						  |
	//|	1	|	0				|		ErrWrongID = 1							| E001							  |
	//|	2	|	0				|		ErrWrongType = 2						| E002							  |
	//|	3	|	0				|		ErrWrongDestination = 3					| E003							  |
	//|	4	|	0				|		ErrWrongSource = 4						| E004							  |
	//|	5	|	0				|		ErrWrongPayload = 5						| E005							  |
	//|	6	|	0				|		ErrInternalServer = 6					| E006							  |
	//|	7	|	0				|		ErrAPI = 7								| E007							  |
	//|	8	|	0				|		ErrKafka = 8							| E008							  |
	//|	9	|	0				|		ErrNats = 9								| E009							  |
	//|	10	|	0				|		ErrWrongID = 10							| E0010							  |
	//|	11	|	0				|		Default = 1								| E000							  |
	//-------------------------------------------------------------------------------------------------------------
	err0 := NewErrorMessage("0", 0)
	if err0 == nil {
		t.Errorf("Gia tri default tao message nil")
	}
	err1 := NewErrorMessage("0", ErrWrongID)
	if err1.ErrCode != "E001" {
		t.Errorf("Must recheck Message Type")
	}
	err2 := NewErrorMessage("0", ErrWrongType)
	if err2.ErrCode != "E002" {
		t.Errorf("Must recheck Message Type")
	}
	err3 := NewErrorMessage("0", ErrWrongDestination)
	if err3.ErrCode != "E003" {
		t.Errorf("Must recheck Message Type")
	}
	err4 := NewErrorMessage("0", ErrWrongSource)
	if err4.ErrCode != "E004" {
		t.Errorf("Must recheck Message Type")
	}
	err5 := NewErrorMessage("0", ErrWrongPayload)
	if err5.ErrCode != "E005" {
		t.Errorf("Must recheck Message Type")
	}
	err6 := NewErrorMessage("0", ErrAbnormal)
	if err6.ErrCode != "E006" {
		t.Errorf("Must recheck Message Type")
	}
	err7 := NewErrorMessage("0", ErrInternalServer)
	if err7.ErrCode != "E007" {
		t.Errorf("Must recheck Message Type")
	}
	err8 := NewErrorMessage("0", ErrAPI)
	if err8.ErrCode != "E008" {
		t.Errorf("Must recheck Message Type")
	}
	err9 := NewErrorMessage("0", ErrKafka)
	if err9.ErrCode != "E009" {
		t.Errorf("Must recheck Message Type")
	}
	err10 := NewErrorMessage("0", ErrNats)
	if err10.ErrCode != "E010" {
		t.Errorf("Must recheck Message Type")
	}
	err00 := NewErrorMessage("0", 1000)
	if err00.ErrCode != "E000" {
		t.Errorf("Must recheck Message Type")
	}
}

func TestConvert(t *testing.T) {
	payloadText := json.RawMessage(`{"foo":"bar"}`)
	msg := Message{
		ID:          "789",
		Type:        "tex",
		Destination: "2",
		Source:      "1",
		Payload:     &payloadText,
	}

	bt := ConvertMessageToMessageQueue(&msg, "xin chao")
	if bt.ClientID != "xin chao" {
		t.Errorf("Must recheck ConvertMessageToMessageQueue")
	}
	cb := ConvertMessageQueueToMessage(bt)
	if cb.ID != msg.ID {
		t.Errorf("Must recheck ConvertMessageToMessageQueue")
	}
}
