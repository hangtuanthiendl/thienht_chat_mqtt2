package websocket

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/websocket"
)

// TestNew
func TestSendMessage(t *testing.T) {
	// Initialize new server
	s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		out := make(chan []byte)
		defer close(out)
		client := New(w, r)

		go client.ReceiveMessage(out)
		for {
			m2 := <-out
			m := make(map[string]interface{})
			json.Unmarshal(m2, &m)

			//t.Logf(m["text"].(string))
			//client.SendMessage(m)
		}
	}))

	defer s.Close()
	// Convert http to ws
	u := "ws" + strings.TrimPrefix(s.URL, "http") + "/chat"

	// // Connect to the server
	ws, _, err := websocket.DefaultDialer.Dial(u, nil)
	if err != nil {
		t.Fatalf("%v", err)
	}
	//defer ws.Close()

	checkID := []struct {
		Text string `json:"text"`
	}{
		{
			Text: "xin chao",
		},
		{
			Text: "xin chao2",
		},

		{
			Text: "xin chao3",
		},
		{
			Text: "xin chao4",
		},
	}
	for _, tt := range checkID {
		ws.WriteJSON(tt)
	}
}

func TestReceiveMessage(t *testing.T) {
	// Initialize new server
	s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		out := make(chan []byte)
		defer close(out)
		client := New(w, r)

		check := []struct {
			Text string `json:"text"`
		}{
			{
				Text: "server 1",
			},
			{
				Text: "server 1",
			},

			{
				Text: "server 1",
			},

			{
				Text: "server 1",
			},
		}
		for _, tt := range check {
			client.SendMessage(tt)
		}

	}))

	defer s.Close()
	// Convert http to ws
	u := "ws" + strings.TrimPrefix(s.URL, "http") + "/chat"

	// // Connect to the server
	ws, _, err := websocket.DefaultDialer.Dial(u, nil)
	if err != nil {
		t.Fatalf("%v", err)
	}
	//defer ws.Close()

	for i := 0; i < 10; i++ {
		_, p, err := ws.ReadMessage()
		if err != nil {
			t.Fatalf("%v", err)
		}
		if p == nil {
			t.Fatalf("a")
		}
		// m := make(map[string]interface{})
		// json.Unmarshal(p, &m)

		// if m["text"].(string) != "" {
		// 	t.Logf("ERRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR")
		// }
		//return
	}
}
