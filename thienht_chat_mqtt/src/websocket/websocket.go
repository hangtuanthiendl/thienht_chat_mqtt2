package websocket

import (
	"bytes"
	"log"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

// Config for connection
const (
	// Maximum message size allowed from peer.
	wsMaxMessageSize = 1048576

	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 3 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

// WsUpgrader - Upgrader for websocket
var WsUpgrader = websocket.Upgrader{
	ReadBufferSize:    40960000,
	WriteBufferSize:   40960000,
	EnableCompression: true,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// Socket -
type Socket struct {
	// The Mutex lock (Concurent task)
	sync.Mutex
	// Websocket conenction
	conn *websocket.Conn
	//
	// Buffered channel of outbound messages
	SendQueue chan interface{}
	// Buffered channel of inbound messages
}

// New - Connect to Websocket
func New(w http.ResponseWriter, r *http.Request) *Socket {
	respHeader := make(http.Header)
	respHeader.Add("Version", os.Getenv("WS_VERSION"))
	respHeader.Add("Sec-WebSocket-Protocol", r.Header.Get("Sec-Websocket-Protocol"))

	conn, err := WsUpgrader.Upgrade(w, r, respHeader)
	if err != nil {
		log.Println(err)
	}

	s := &Socket{
		conn:      conn,
		SendQueue: make(chan interface{}), //2MB
	}
	return s
}

// ReceiveMessage - Send message to webscoket
func (ws *Socket) ReceiveMessage(out chan []byte, closeWs chan *Socket) {
	ws.conn.SetReadLimit(wsMaxMessageSize)
	ws.conn.SetReadDeadline(time.Now().Add(pongWait))
	ws.conn.SetPongHandler(func(string) error {
		//log.Println("PONG")
		ws.conn.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})
	for {
		_, message, err := ws.conn.ReadMessage()
		if err != nil {
			log.Println(err)
			closeWs <- ws
			return
		}
		message = bytes.TrimSpace(bytes.Replace(message, newline, space, -1))
		out <- message
	}
}

// SendMessage - Send Message to Client
func (ws *Socket) SendMessage(closeWs chan *Socket) { //TODO check typ
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
	}()

	for {
		select {
		case message, ok := <-ws.SendQueue:
			if !ok {
				ws.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}
			ws.Lock()
			ws.conn.SetWriteDeadline(time.Now().Add(writeWait))
			err := ws.conn.WriteJSON(message)
			if err != nil {
				log.Println("Failed to write data to Websocket: %v", err)
				return
			}
			ws.Unlock()
		case <-ticker.C:
			ws.conn.SetWriteDeadline(time.Now().Add(writeWait))
			//log.Println("PING")
			if err := ws.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				closeWs <- ws
				return
			}
		}
	}
}

// Close - x
func (ws *Socket) Close() {
	ws.conn.Close()
}
