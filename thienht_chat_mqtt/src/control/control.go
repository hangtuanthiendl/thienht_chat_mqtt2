package control

import (
	cAPI "chat/src/api"
	cClient "chat/src/client"
	message "chat/src/message"
	cStream "chat/src/stream"
	cSocket "chat/src/websocket"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

const (
	defaultChannel = 0 // Type of default subscribe
	durableChannel = 1 // Type of durable subscribe
)

// ServeWs - When websocket connect to websocket
func ServeWs(hub *cClient.Hub, w http.ResponseWriter, r *http.Request) {
	// Create connection for WS
	ws := cSocket.New(w, r)
	// CloseWSQueue - X
	CloseWSQueue := make(chan *cSocket.Socket)
	defer func() {
		close(CloseWSQueue)
	}()
	//Send message to client
	go ws.SendMessage(CloseWSQueue)
	// New API
	api, err := cAPI.New(r)
	if err != nil {
		ws.SendQueue <- message.NewErrorMessage("#ERROR#", message.ErrLoginFailed)
		time.Sleep(2 * time.Second)
		ws.Close()
		return
	}
	// Check if the client exist or not
	isFirst, cID, lstChannels, err := api.Check()
	if err != nil {
		cID = 0
		log.Println("Login problems")
		ws.SendQueue <- message.NewErrorMessage("#ERROR#", message.ErrLoginFailed)
		time.Sleep(2 * time.Second)
		ws.Close()
		return
	}
	clientID := fmt.Sprint(cID)

	log.Println(">>>>>>>>>>>>>>>>>>>>>>>>NEW SESSION<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
	log.Println(cID)
	log.Println(isFirst)
	log.Println(lstChannels)

	//hub.Hstorage.UnLockClient(clientID)
	//Check Lock
	// if hub.Hstorage.CheckLock(clientID) == true {
	// 	cID = 0
	// 	log.Println("USER LOCKED!!!!!")
	// 	ws.SendQueue <- message.NewErrorMessage("#ERROR#", message.ErrLocked)
	// 	time.Sleep(1 * time.Second)
	// 	ws.Close()
	// 	return
	// }

	if cID > 0 {
		hub.Hstorage.LockClient(clientID)

		// Which partion of client
		numPartion, _ := strconv.Atoi(os.Getenv("HAZELCAST_NUMBER_PARTION"))
		partionID := cID % numPartion
		// Add client to list client on
		var client *cClient.Client = hub.Clients[clientID]
		if client == nil {
			stream := cStream.New(clientID)
			if stream == nil {
				ws.SendQueue <- message.NewErrorMessage("#ERROR#", message.ErrLocked)
				time.Sleep(1 * time.Second)
				ws.Close()
				return
			}
			client = cClient.New(hub, ws, stream, clientID, partionID, api)
			hub.Register <- client
			// Subscribe default channel
			go stream.SubscribleChannels(client.NatQueueOut, durableChannel, []string{os.Getenv("NATS_DEFAULT_CHANNEL") + clientID})
			// 2. Subscrible to historical channel
			go stream.SubscribleChannels(client.NatQueueOut, durableChannel, lstChannels)
		} else {
			client.Websocket.PushBack(ws)
		}
		// ************************************************************************************************
		// - Wait message from client
		go ws.ReceiveMessage(client.ReceiveQueue, CloseWSQueue)
		// - Sync queue message
		go client.SyncMessageByChannel(lstChannels, isFirst)
		// - Sync history message
		go client.SyncMessageByUser()
		// - Direct message to websocket
		go client.ControlWsMessage()
		// - Direct message to nat streaming
		go client.ControlStreamMessage()

		for {
			ws := <-CloseWSQueue
			for e := client.Websocket.Front(); e != nil; e = e.Next() {
				if e.Value == ws {
					log.Println("Close websocket!!!" + clientID)
					client.Websocket.Remove(e)
					ws.Close()
					return
				}
			}
			if client.Websocket.Len() == 0 {
				hub.Unregister <- client
			}
		}
	}

}
