package main

import (
	"log"
	"net/http"
	"os"

	client "chat/src/client"
	control "chat/src/control"

	"github.com/joho/godotenv"
)

func main() {
	// Import config to environment variables
	err := godotenv.Load("config.env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	// Create log file
	f, err := os.OpenFile("log/log.mwg", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Panic(err)
	}
	defer f.Close()

	log.SetOutput(f)
	// Control clients: add new client to list or remove it
	hub := client.NewHub()
	go hub.Run()

	// Handle web app client
	http.HandleFunc("/chat", func(w http.ResponseWriter, r *http.Request) {
		// log.Println(r)
		control.ServeWs(hub, w, r)
	})

	//Other function
	http.Handle("/", http.FileServer(http.Dir("./assets")))
	log.Println("Serving at localhost:80...")
	log.Println(http.ListenAndServe(":80", nil))
}
