#!/bin/bash

status=`curl -s -o /dev/null -w "%{http_code}" https://example.com`
echo `date` $status >> crash_checks.log

if [ "$status" -ne "200" ]
then
       # Take any appropriate recovery action here.
	echo "webserver seems down, initiating reboot." >> check.log
	sudo reboot
fi


#!/bin/bash
#Scripts to start services if not running
#Specify DMGR Path Here
DMGR=/opt/IBM/WebSphere/AppServer/profiles/Dmgr01
#Specify Profile Path Here
Profile=/opt/IBM/WebSphere/AppServer/profiles/AppSrv01
ps -ef | grep dmgr |grep -v grep > /dev/null
if [ $? != 0 ]
then
       $DMGR/bin/startManager.sh > /dev/null
fi
ps -ef | grep nodeagent |grep -v grep > /dev/null
if [ $? != 0 ]
then
       $Profile/bin/startNode.sh > /dev/null
fi
ps -ef | grep server1 |grep -v grep > /dev/null
if [ $? != 0 ]
then
       $Profile/bin/startServer.sh server1 > /dev/null
fi